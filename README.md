# NimbleLightning

This project aims to gather, prepare and analyse data related to blockchain second layer networks.

## Structure

- Analysis: contains the scripts to perform different analysis of the state of the network visible at the second layer

## Usage

Most of the provided scripts are written in python. Each folder contains a `requirements.txt` files which specifies all the python libraries required to run the scripts in that specific folder.

To have a better experience the usage of a virtual environment is strongly advised. 
 
