#!/usr/bin/env python3
"""Analyze the result of a probe

Failcodes:

 - -1 indicates that we were unable to find a route to the destination. This
    usually indicates that this is a leaf node that is currently offline.

 - 17 0x0011 INVALID 17

 - 21 0x0015 WIRE_EXPIRY_TOO_FAR

 - 4103 0x1007 WIRE_TEMPORARY_CHANNEL_FAILURE Payment excess balance in that direction

 - 4110 0x100e WIRE_EXPIRY_TOO_SOON

 - 4116 0x1014 WIRE_CHANNEL_DISABLED

 - 16392 0x4008

 - 16394 0x400a WIRE_UNKNOWN_NEXT_PEER

 - 16399 0x400f is the code for unknown payment details and indicates a successful
   probe. The destination received the incoming payment but could not find a
   matching `payment_key`, which is expected since we generated the
   `payment_hash` at random :-)

"""

import sys
from collections import defaultdict

import networkx as nx
import matplotlib.pyplot as plt
from matplotlib.patches import Patch

from constants import *

fail_codes_to_colors_mapping = {
    NOT_PROBED: 'black',
    NO_ROUTE_TO_HOST: 'black',
    17: 'orange',
    21: 'orange',
    PROBE_MISSING_FEATURE: 'orange',
    PROBE_WIRE_TEMPORARY_CHANNEL_FAILURE: 'orange',
    4110: 'orange',
    4116: 'orange',
    16392: 'red',
    PROBE_WIRE_UNKNOWN_NEXT_PEER: 'red',
    PROBE_SUCCESS: 'green',
    PROBE_MYSELF: 'blue',
}

fail_codes_to_label = {
    NOT_PROBED: 'Not probed',
    NO_ROUTE_TO_HOST: 'No route to host',
    17: 'INVALID 17',
    21: 'WIRE_EXPIRY_TOO_FAR',
    PROBE_MISSING_FEATURE: 'Missing feature',
    PROBE_WIRE_TEMPORARY_CHANNEL_FAILURE: 'WIRE_TEMPORARY_CHANNEL_FAILURE',
    4110: 'WIRE_EXPIRY_TOO_SOON',
    4116: 'WIRE_CHANNEL_DISABLED',
    16392: 'Unknown error code',
    PROBE_WIRE_UNKNOWN_NEXT_PEER: 'WIRE_UNKNOWN_NEXT_PEER',
    PROBE_SUCCESS: 'Working',
}

good_to_bad_failcodes = [PROBE_MYSELF,
                         PROBE_SUCCESS,
                         17,
                         21,
                         PROBE_MISSING_FEATURE,
                         PROBE_WIRE_TEMPORARY_CHANNEL_FAILURE,
                         4110,
                         4116,
                         16392,
                         PROBE_WIRE_UNKNOWN_NEXT_PEER,
                         NO_ROUTE_TO_HOST, NOT_PROBED]


def draw_failcode_histogram(label, items):
    histogram = defaultdict(int)
    for e, edge_data in items:
        histogram[edge_data['fail_code']] += 1
    sizes = [v for (k, v) in histogram.items()]
    explode = list(map(lambda x: 0.1 if x == PROBE_SUCCESS else 0, histogram))
    labels = [fail_codes_to_label[fail_code] for fail_code in histogram.keys()]
    fig1, ax1 = plt.subplots()
    plt.title(label)
    ax1.pie(sizes,
            explode=explode,
            labels=labels,
            autopct=lambda p: '{:.2f}%({:.0f})'.format(p, (p / 100) * sum(sizes)),
            startangle=90)
    ax1.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.


def draw_graph(g):
    # Pre process the graph so that each node has a fail code
    preprocess_fail_codes(g)

    # Draw the Graph of nodes
    node_colors = [fail_codes_to_colors_mapping[g.nodes[n]['fail_code']] for n in g.nodes()]
    edge_colors = [fail_codes_to_colors_mapping[g.edges[n]['fail_code']] for n in g.edges()]

    plt.figure(figsize=(12, 12))
    pos = nx.random_layout(g)

    nx.draw_networkx_edges(g, pos, edgelist=g.edges(), edge_color=edge_colors,
                           cmap=plt.cm.jet, arrows=True)
    nx.draw_networkx_nodes(g, pos, nodelist=g.nodes(), node_color=node_colors,
                           node_size=100, cmap=plt.cm.jet)

    capacities_dict = nx.get_edge_attributes(g, 'capacity_usd')
    # Convert edges from float to string
    edge_labels = {}
    for src, dest, data in g.edges(data=True):
        edge_labels[(src, dest)] = f"{data['capacity_usd']}$ - {data['short_channel_id']}"
    nx.draw_networkx_edge_labels(g, pos, edge_labels=edge_labels)

    plt.legend(loc="center left", bbox_to_anchor=(1.05, 1),
               handles=[
                   Patch(color='blue', label='My node'),
                   Patch(color='green', label='Reachable'),
                   Patch(color='black', label='No route to host'),
                   Patch(color='orange', label='Temporary failure'),
                   Patch(color='red', label='Permanent failure'),
               ])
    plt.title("Reachable nodes in Lightning Network")
    plt.axis('off')


def preprocess_fail_codes(G):
    """
    Ensures that each node has a 'fail_code' attribute
    :param G: Graph that needs to be modified
    :return:
    """
    for n in G.nodes():
        if 'fail_code' not in G.nodes[n]:
            G.nodes[n]['fail_code'] = NOT_PROBED
        if 'fail_codes' in G.nodes[n]:
            for fail_code in good_to_bad_failcodes:
                if fail_code in G.nodes[n]['fail_codes']:
                    G.nodes[n]['fail_code'] = fail_code


def exclude_inferred(g: nx.Graph) -> nx.Graph:
    inferred_edges = []

    for u, v, inferred in g.edges(data='inferred'):
        if inferred:
            inferred_edges.append((u, v))
    g.remove_edges_from(inferred_edges)
    return g


if __name__ == '__main__':

    if len(sys.argv) != 2:
        print("Usage: python3 draw_nodes.py <input_file_path>")
        sys.exit(1)
    input_file_path = sys.argv[1]

    g = nx.read_gpickle(input_file_path)

    include_inferred = False
    if include_inferred:
        print("Including the inferred edges in the calculation")
    else:
        print("EXCLUDING inferred edges")
        g = exclude_inferred(g)

    draw_graph(g)

    # Draw the frequencies of nodes and edges
    draw_failcode_histogram(label="Working channels in Lightning Network", items=g.edges.items())

    g.remove_node("0318ec925201449f1a026beb08af37347ae3b35995c732df0357b807a3a59f2044")

    draw_failcode_histogram(label="Reachable nodes in Lightning Network", items=g.nodes.items())
    plt.show()
