#!/bin/bash

PID_FILE=/Users/lambrosini/.lightning/lightningd-bitcoin.pid

if [[ -f "$PID_FILE" ]]; then
    echo "$PID_FILE exist. Trying to shutdown current instance Shutting down current instance"
    lightning-cli --network=bitcoin stop
    echo "Done."
fi
echo "Launching lightningd with the probing plugin."
source /Users/lambrosini/IdeaProjects/SUPSI_work/nimble/venv/bin/activate && lightningd  --network=bitcoin --plugin=/Users/lambrosini/IdeaProjects/SUPSI_work/nimble/plugins/probing.py --plugin=/Users/lambrosini/IdeaProjects/SUPSI_work/nimble/plugins/counters.py