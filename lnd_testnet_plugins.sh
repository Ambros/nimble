#!/bin/bash

PID_FILE=/Users/lambrosini/.lightning/lightningd-testnet.pid

if [[ -f "$PID_FILE" ]]; then
  echo "$PID_FILE exist. Shutting down current instance"
  lightning-cli --testnet stop
fi

echo "Launching lightningd with the probing plugin."
source /Users/lambrosini/IdeaProjects/SUPSI_work/nimble/venv/bin/activate && /Users/lambrosini/Bitcoin/lightning/lightningd/lightningd --testnet --plugin=/Users/lambrosini/IdeaProjects/SUPSI_work/nimble/plugins/probing.py --plugin=/Users/lambrosini/IdeaProjects/SUPSI_work/nimble/plugins/counters.py
