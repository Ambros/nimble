DEFAULT_PROBE_AMOUNT = 10_000 * 1_000 * 1  # Roughly 1$
DEFAULT_PRECISION = 10_000 * 1_000 * 1  # Roughly 1$
MIN_LN_CHANNEL_CAPACITY = 10_000 * 1_000 * 1  # Roughly 1$
MAX_LN_CHANNEL_CAPACITY = 16_000_000_000  # 0.16 BTC, which is the technical imposed limit

MSAT_TO_BTC = 0.00000000001
BTC_TO_USD = 10_000

BASE_PAYMENT_AMOUNT = 12000

# Fail codes
PROBE_MYSELF = 0
PROBE_SUCCESS = 16399
PROBE_WIRE_TEMPORARY_CHANNEL_FAILURE = 4103
PROBE_MISSING_FEATURE = 16393
PROBE_WIRE_UNKNOWN_NEXT_PEER = 16394
# Custom fail codes
NO_ROUTE_TO_HOST = -1
NOT_PROBED = -2
PROBE_OPPOSITE = -3
