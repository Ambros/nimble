#!/usr/bin/env python3
import networkx
from pyln.client import Plugin

from logic.graph import channels_to_graph
from logic.common import get_my_node_id

p = Plugin()


@p.method("countchannels")
def count_channels(plugin):
    """

    Return the number of channels on the network I am aware of

    """
    return len(plugin.rpc.listchannels()['channels'])


@p.method("nodes-at-distance")
def on_node_at_distance(plugin, distance):
    """

    Return the identifier of the nodes at a determined distance

    """
    channels = plugin.rpc.listchannels()['channels']
    G = channels_to_graph(channels)
    my_node_id = get_my_node_id(plugin.rpc)
    ids = []
    source_path_lengths = networkx.single_source_dijkstra_path_length(G, my_node_id, cutoff=distance)
    for vertex, length in source_path_lengths.items():
        if length == distance:
            ids.append(vertex)
    return ids


@p.method("countnodes")
def count_nodes(plugin):
    """

    Return the number of nodes on the network I am aware of

    """
    res = plugin.rpc.listnodes()
    n = len(res['nodes'])
    return n


@p.method("countmychannels")
def count_my_channels(plugin):
    """

    Return the number of channels that you node has

    """
    peers = plugin.rpc.listpeers()['peers']
    return sum(len(p['channels']) for p in peers)


@p.method("countpeers")
def count_peers(plugin):
    """

    Return the number of peers that you node is connected to

    """
    return len(plugin.rpc.listpeers()['peers'])


@p.init()
def init(options, configuration, plugin):
    plugin.log("Plugin counters.py initialized")


p.run()
