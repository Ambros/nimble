import string
from random import choice
from typing import List, Dict

import networkx as nx

from pyln.client import RpcError

from constants import *
from model.probe import Probe
from logic.common import select_route, try_route, msat_to_usd
from logic.exceptions import NodeNotReachable, ZeroCapacity


def build_route(previous_route, last_channel_sid, dest_node_id, fee_to_add, delay_to_add):
    """

    :param delay_to_add:
    :param fee_to_add:
    :param previous_route: c-lightning route to arrive to the first endpoint of the channel to probe
    :param last_channel_sid: unique identifier of the channel to probe
    :param dest_node_id: id of the endpoint of the node
    :return:
    """
    last_hop = [{
        "id": dest_node_id,
        "channel": last_channel_sid,
        # "direction": 0,  # Random? I try and otherwise change?
        "msatoshi": BASE_PAYMENT_AMOUNT,  # Does not really matter
        # "amount_msat": "12000msat",  # Does not really matter
        "delay": 9,
        "style": "tlv"
    }]
    # Update the previous pieces of the route
    for piece in previous_route:
        piece["msatoshi"] += fee_to_add
        piece["delay"] += delay_to_add
    return previous_route + last_hop


def get_initial_probes(G, my_node_id) -> List[Dict]:
    pending_probes = []
    for n in nx.neighbors(G, n=my_node_id):
        sid = G[my_node_id][n]['short_channel_id']
        pending_probes.append({
            "dest_node_id": n,
            "short_channel_id": sid,
            "route_known_capacity": MAX_LN_CHANNEL_CAPACITY,
            "previous_route": [],
            **G[my_node_id][n]
        })
    return pending_probes


def probe_capacity(plugin,
                   dest_node_id,
                   designated_route=[],
                   precision=DEFAULT_PRECISION,
                   min_capacity=MIN_LN_CHANNEL_CAPACITY,
                   max_capacity=MAX_LN_CHANNEL_CAPACITY) -> Probe:
    """

    :param designated_route:
    :param plugin:
    :param dest_node_id:
    :param precision: expressed in msat
    :param min_capacity: expressed in msat
    :param max_capacity:  expressed in msat
    :return:
    """
    debug = False
    if debug:
        plugin.log(f"Starting capacity probing with precision {msat_to_usd(precision):.2f} USD")
    payment_hash = ''.join(choice(string.hexdigits) for _ in range(64))
    probe = Probe(destination=dest_node_id, designated_route=designated_route, payment_hash=payment_hash)

    # Try to find a route with the minimum capacity to understand if we can theoretically reach the node
    try:
        temp_route = select_route(plugin=plugin, node_id=dest_node_id, msatoshi=min_capacity)
        if len(designated_route) != len(temp_route):
            plugin.log("Selected and designated route differs. Please look into it.")
            raise ZeroCapacity(probe=probe)
    except RpcError:  # Cannot find a route to host or something is wrong in the RPC request
        probe.failcode = NO_ROUTE_TO_HOST
        raise NodeNotReachable(probe=probe)

    capacities = range(min_capacity, max_capacity, precision)

    # Try to execute payments with different amounts
    left = 0
    right = len(capacities) - 1

    while left <= right:
        mid = left + (right - left) // 2
        c = capacities[mid]
        if debug:
            plugin.log(f"Testing capacity {c} msat({msat_to_usd(c):.2f}) USD")
        try:
            probe.selected_route = select_route(plugin=plugin, node_id=dest_node_id, msatoshi=c)
            plugin.log(f"Selected route:   {','.join([r['channel'] for r in probe.selected_route])}")
        except RpcError:
            if debug:
                plugin.log(f"No route available with this capacity. Decreasing.")
            right = mid - 1
            continue
        try_route(plugin=plugin, probe=probe, exclude_erring=False)
        if probe.failcode == PROBE_SUCCESS:  # We have to grow the capacity again
            if debug:
                plugin.log(f"Enough capacity. Increasing {probe.failcode}")
            left = mid + 1
            continue
        # We have to decrease the capacity
        if probe.failcode == PROBE_WIRE_TEMPORARY_CHANNEL_FAILURE or probe.failcode == PROBE_MISSING_FEATURE:
            if debug:
                plugin.log(f"NOT available. Decreasing. Failcode:{probe.failcode}")
            right = mid - 1
            continue
        if probe.failcode == PROBE_WIRE_UNKNOWN_NEXT_PEER:
            if debug:
                plugin.log(f"Cannot reach the next peer. Possibly wrong direction.")
            probe.capacity_usd = 0
            probe.capacity_msat = 0
            raise ZeroCapacity(probe)
        else:
            if debug:
                plugin.log(f"Unexpected error code {probe.failcode}")
            probe.capacity_usd = 0
            probe.capacity_msat = 0
            raise ZeroCapacity(probe)
    if right == -1:  # The smaller capacity is too high
        probe.capacity_usd = 0
        probe.capacity_msat = 0
        raise ZeroCapacity(probe)

    # Everything went for the best. We compute the results
    determined_capacity_msat = int((capacities[left] + capacities[right]) / 2)
    probe.capacity_msat = determined_capacity_msat
    probe.capacity_usd = msat_to_usd(determined_capacity_msat)
    return probe


def probes_to_edge_attributes_multiple(G, channels, probes: List[Probe], plugin):
    # Build a map with the short_channel_id as key
    channels_map = {}
    for c in channels:
        channels_map[c['short_channel_id'] + "/1"] = (c['source'], c['destination'], c['satoshis'])
        channels_map[c['short_channel_id'] + "/0"] = (c['destination'], c['source'], c['satoshis'])
    # Add attributes to the graph EDGES
    channels_fail_codes = {}
    for p in probes:
        if p.selected_route is not None:
            sid = p.selected_route[len(p.selected_route) - 1]['channel']
            direction = p.selected_route[len(p.selected_route) - 1]['direction']
            directed_channel_id = f"{sid}/{direction}"
        else:
            plugin.log(f"Probe {p} does not have a selected route.")
            directed_channel_id = p.designated_route[len(p.designated_route) - 1]  # TODO In case it is
            plugin.log(f"Using {directed_channel_id} to mitigate the issue")
        src, dest, sats = channels_map[directed_channel_id]
        channels_fail_codes[(src, dest)] = {'fail_code': p.failcode,
                                            'inferred': False,
                                            'capacity_msat': p.capacity_msat,
                                            'capacity_usd': p.capacity_usd
                                            }
        inverse_capacity_msat = sats * 1000 - p.capacity_msat
        channels_fail_codes[(dest, src)] = {'fail_code': p.failcode,
                                            'inferred': True,
                                            'capacity_msat': inverse_capacity_msat,
                                            'capacity_usd': msat_to_usd(inverse_capacity_msat)
                                            }
    # Set the attributes for the edges we have not probed
    for e in G.edges:
        if e not in channels_fail_codes:
            channels_fail_codes[e] = {
                'fail_code': NOT_PROBED,
                'capacity_msat': 0,
                'capacity_usd': 0
            }
    return channels_fail_codes


def probes_to_node_attributes_multiple(my_node_id: str, probes):
    node_fail_codes = {my_node_id: {'fail_codes': [PROBE_MYSELF]}}
    for p in probes:
        if p.destination not in node_fail_codes:
            node_fail_codes[p.destination] = {'fail_codes': [p.failcode]}
        else:
            node_fail_codes[p.destination]['fail_codes'].append(p.failcode)
    return node_fail_codes
