from time import time
from typing import List

from pyln.client import RpcError

from constants import MSAT_TO_BTC, BTC_TO_USD
from model.probe import Probe

permanent_exclusions = []
custom_exclusions = []
temporary_exclusions = {}


def msat_to_usd(msat: int) -> float:
    """
    Converts milli satoshis to USD
    :param msat:
    :return:
    """
    return msat * MSAT_TO_BTC * BTC_TO_USD


def clear_temporary_exclusion(plugin):
    global temporary_exclusions
    timed_out = [k for k, v in temporary_exclusions.items() if v < time()]
    for k in timed_out:
        del temporary_exclusions[k]

    plugin.log("Removed {}/{} temporary exclusions.".format(
        len(timed_out), len(temporary_exclusions))
    )


def overwrite_allowed(all_channels: List[str], allowed: List[str]):
    global custom_exclusions
    all_channels = set(all_channels)
    allowed = set(allowed)
    custom_exclusions = list(all_channels.symmetric_difference(allowed))


def try_route(plugin, probe: Probe, exclude_erring: bool):
    """
    Tries to execute a payment on the route provided in the Probe. Directly modifies the probe with the outcome
    :param exclude_erring: If true the channel in error will be appended to the exclusions
    :param plugin:
    :param probe:
    :return:
    """
    # Send the probe and poll for its response
    sendpay_res = plugin.rpc.sendpay(probe.selected_route, probe.payment_hash)
    try:
        waitsendpay_res = plugin.rpc.waitsendpay(probe.payment_hash)
        probe.failcode = waitsendpay_res['failcode']
        plugin.log(f"Payment failed with code {probe.failcode} ({waitsendpay_res['failcodename']})")
    except RpcError as e:
        error = e.error['data']
        probe.failcode = e.error['data']['failcode']
        # plugin.log(f"ERR: Payment failed with code {probe.failcode} ({e.error['data']['failcodename']})")
        plugin.log(f"ERR: Payment failed with code {probe.failcode}")

        # If I do have an error I also update the
        if exclude_erring:
            erring_channel = error['erring_channel']
            erring_direction = error['erring_direction']
            update_exclusions(plugin=plugin,
                              erring_channel=erring_channel,
                              erring_direction=erring_direction,
                              fail_code=probe.failcode)


def update_exclusions(plugin, erring_channel, erring_direction, fail_code):
    exclusion = f"{erring_channel}/{erring_direction}"
    if fail_code in [16392, 16394]:
        plugin.log(f'Adding exclusion for channel {exclusion} ({len(permanent_exclusions)} total))')
        permanent_exclusions.append(exclusion)
    if fail_code in [21, 4103]:
        plugin.log(f'Adding temporary exclusion for channel {exclusion} ({len(temporary_exclusions)} total))')
        expiry = time() + plugin.probe_exclusion_duration
        temporary_exclusions[exclusion] = expiry


def get_my_node_id(rpc) -> str:
    """
    Returns the unique identifier of the node performing the RPC
    :param rpc: rpc interface to perform the request
    :return: the identifier as str
    """
    return rpc.getinfo()['id']


def select_route(plugin, node_id, msatoshi):
    return plugin.rpc.getroute(
        node_id,
        msatoshi=msatoshi,
        riskfactor=1,
        exclude=permanent_exclusions + list(temporary_exclusions.keys()) + custom_exclusions)['route']
