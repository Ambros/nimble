class NodeNotReachable(Exception):

    def __init__(self, probe=None):
        self.probe = probe


class ZeroCapacity(Exception):

    def __init__(self, probe):
        self.probe = probe