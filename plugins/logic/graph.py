from typing import List

from networkx import DiGraph


def channels_to_graph(channels: List) -> DiGraph:
    G = DiGraph()
    for c in channels:
        weight = 1
        if c['active'] is True:
            G.add_edge(c['source'], c['destination'],
                       short_channel_id=c['short_channel_id'],
                       public=c["public"],
                       capacity=c['satoshis'],
                       last_update=c['last_update'],
                       base_fee_millisatoshi=c['base_fee_millisatoshi'],
                       fee_per_millionth=c['fee_per_millionth'],
                       delay=c['delay'],
                       weight=weight)
            # listchannels returns an undirected graph. We transform it to a directed one doubling all the edges
            G.add_edge(c['destination'], c['source'],
                       short_channel_id=c['short_channel_id'],
                       public=c["public"],
                       capacity=c['satoshis'],
                       last_update=c['last_update'],
                       base_fee_millisatoshi=c['base_fee_millisatoshi'],
                       fee_per_millionth=c['fee_per_millionth'],
                       delay=c['delay'],
                       weight=weight)
    return G


def obtain_subgraph(G, nodes_to_probe) -> DiGraph:
    nodes_to_remove = []

    for n in G.nodes():
        if n not in nodes_to_probe:
            nodes_to_remove.append(n)
    G.remove_nodes_from(nodes_to_remove)
    return G
