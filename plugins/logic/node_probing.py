def probes_to_edge_attributes(G, channels, probes):
    # Build a map with the short_channel_id as key
    channels_map = {}
    for c in channels:
        channels_map[c['short_channel_id'] + "/1"] = (c['source'], c['destination'])
        channels_map[c['short_channel_id'] + "/0"] = (c['destination'], c['source'])
    # Add attributes to the graph EDGES
    channels_fail_codes = {}
    for p in probes:
        if p.route is None:
            continue
        for route_segment in p.route:
            directed_channel_id = f"{route_segment['channel']}/{route_segment['direction']}"
            src, dest = channels_map[directed_channel_id]
            channels_fail_codes[(src, dest)] = {'fail_code': p.failcode}
    # Set the attributes for the edges we have not probed
    for e in G.edges:
        if e not in channels_fail_codes:
            channels_fail_codes[e] = {'fail_code': -2}
    return channels_fail_codes


def probes_to_node_attributes(my_node_id, probes):
    node_fail_codes = {my_node_id: {'fail_code': 0}}
    for p in probes:
        node_fail_codes[p.destination] = {'fail_code': p.failcode}
    return node_fail_codes
