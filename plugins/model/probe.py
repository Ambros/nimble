import time
from typing import List


class Probe:
    def __init__(self,
                 destination: str,
                 selected_route: str = None,
                 designated_route: List[str] = [],
                 error: str = None,
                 capacity_msat: int = -1,
                 capacity_usd: int = -1,
                 erring_channel: str = "",
                 erring_direction: str = "",
                 failcode: int = -99,
                 payment_hash: str = "",
                 started_at: int = 0,
                 finished_at: int = 0,
                 ):
        self.destination = destination
        self.designated_route = designated_route
        self.selected_route = selected_route
        self.capacity_msat = capacity_msat
        self.capacity_usd = capacity_usd
        self.error = error
        self.erring_channel = erring_channel
        self.erring_direction = erring_direction
        self.failcode = failcode
        self.payment_hash = payment_hash
        if started_at == 0:
            self.started_at = time.time()
        else:
            self.started_at = started_at
        self.finished_at = finished_at

    def to_json(self):
        if self.selected_route is None:
            route = "Not available"
        else:
            route = ','.join([r['channel'] for r in self.selected_route])
        return {
            'destination': self.destination,
            'selected_route': route,
            'designated_route': ','.join([r for r in self.designated_route]),
            'erring_channel': self.erring_channel,
            'erring_direction': self.erring_direction,
            'failcode': self.failcode,
            'capacity_usd': self.capacity_usd,
            'capacity_msat': self.capacity_msat,
            'payment_hash': self.payment_hash,
            'started_at': str(self.started_at),
            'finished_at': str(self.finished_at),
        }
