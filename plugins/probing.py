#!/usr/bin/env python3
"""Plugin that probes the network for failed channels.

This plugin regularly performs a random probe of the network by sending a
payment to a random node in the network, with a random `payment_hash`, and
observing how the network reacts. The random `payment_hash` results in the
payments being rejected at the destination, so no funds are actually
transferred. The error messages however allow us to gather some information
about the success probability of a payment, and the stability of the channels.

The random selection of destination nodes is a worst case scenario, since it's
likely that most of the nodes in the network are leaf nodes that are not
well-connected and often offline at any point in time. Expect to see a lot of
errors about being unable to route these payments as a result of this.

The probe data is stored in a sqlite3 database for later inspection and to be
able to eventually draw pretty plots about how the network stability changes
over time. For now you can inspect the results using the `sqlite3` command
line utility:

```bash
sqlite3  ~/.lightning/probes.db "select destination, erring_channel, failcode from probes"
```

Failcode -1 and 16399 are special:

 - -1 indicates that we were unable to find a route to the destination. This
    usually indicates that this is a leaf node that is currently offline.

 - 16399 is the code for unknown payment details and indicates a successful
   probe. The destination received the incoming payment but could not find a
   matching `payment_key`, which is expected since we generated the
   `payment_hash` at random :-)

"""
import json
import string
from copy import deepcopy
from datetime import timedelta
from random import choice
from typing import Dict, List, Optional

from pyln.client.lightning import RpcError

import networkx as nx
from pyln.client import Plugin
from time import time

from constants import *
from logic.graph import channels_to_graph, obtain_subgraph
from logic.channel_probing import probe_capacity, get_initial_probes, probes_to_node_attributes_multiple, \
    probes_to_edge_attributes_multiple
from logic.node_probing import probes_to_node_attributes, probes_to_edge_attributes
from model.probe import Probe
from logic.common import get_my_node_id, select_route, try_route, overwrite_allowed
from logic.exceptions import NodeNotReachable, ZeroCapacity

p = Plugin()


def get_pending_probe(pending_probes: List[Dict], out_edge_sid: str) -> Optional[Dict]:
    for p in pending_probes:
        if p['short_channel_id'] == out_edge_sid:
            return p
    return None


@p.method('probe-capacity-total')
def on_probe_capacity_total(plugin, distance, precision=DEFAULT_PRECISION):
    """
    Determine the capacity of every channel that we are aware of
    :param distance:
    :param plugin:
    :param precision: granularity of the probing for capacity expressed in msats
    :return:
    """
    my_node_id = get_my_node_id(plugin.rpc)
    channels = plugin.rpc.listchannels()['channels']
    all_channel_ids = [f"{c['short_channel_id']}/0" for c in channels]
    all_channel_ids += [f"{c['short_channel_id']}/1" for c in channels]
    G = channels_to_graph(channels)

    nodes_to_probe = nx.single_source_shortest_path_length(G, source=my_node_id, cutoff=distance)
    G = obtain_subgraph(G, nodes_to_probe)

    edge_count = len(G.edges) - 1
    plugin.log(f"Edges to probe {edge_count}. Estimated time {str(timedelta(seconds=edge_count * 3))} ")

    pending_probes = get_initial_probes(G, my_node_id)

    probed = {}

    while len(pending_probes) > 0:
        plugin.log(f"Pending probes: {len(pending_probes)}")
        curr_probe = pending_probes.pop()
        sid = curr_probe['short_channel_id']

        plugin.log(f"*************************************")
        plugin.log(f"**********  {sid} towards {curr_probe['dest_node_id'][:5]} ************")
        plugin.log(f"*************************************")

        if sid in probed and probed[sid].capacity_msat >= curr_probe['route_known_capacity']:
            plugin.log(
                f"SKIPPING! Reason: This route max capacity is {curr_probe['route_known_capacity']} and capacity is already known to be {probed[sid].capacity_msat}")
            continue

        allowed_ids = [f"{r['channel']}/{r['direction']}" for r in
                       curr_probe["previous_route"]]

        allowed_ids.append(sid + "/0")
        plugin.log(
            f"Designated route: {','.join(allowed_ids)}")  # TODO The direction of the last channel is not reliable
        designated_route = list(allowed_ids)
        allowed_ids.append(sid + "/1")

        # Force c-lightning to follow the route we have indicated
        overwrite_allowed(all_channel_ids, allowed_ids)
        try:
            p = probe_capacity(plugin=plugin,
                               designated_route=designated_route,
                               dest_node_id=curr_probe['dest_node_id'],
                               precision=precision)
            p.failcode = PROBE_SUCCESS
            p.finished_at = time()
            # Store the results only if it lead to a higher capacity
            if sid not in probed:
                probed[sid] = p
            elif p.capacity_msat > probed[sid].capacity_msat:
                probed[sid] = p

            plugin.log(f"Capacity of {sid} is ~{p.capacity_msat} ({p.capacity_usd:.2f}$)")

            # Determine which out_edges of the destination node are still to probe
            plugin.log(f"{curr_probe['dest_node_id']} has {len(G.out_edges(curr_probe['dest_node_id']))} channels")
            for src, dest, data in G.out_edges(curr_probe['dest_node_id'], data=True):
                potential_next_probe_capacity = p.capacity_msat
                out_edge_sid = data['short_channel_id']
                # I have never seen this edge before and I do not have a plan to probe it
                if out_edge_sid not in probed and get_pending_probe(pending_probes, out_edge_sid) is None:
                    plugin.log(f"Adding {out_edge_sid} to pending probes. Reason: Never probed and not in queue.")
                    add_pending_probe(dest, p, pending_probes, out_edge_sid)
                    continue

                # I have already probed it, but going through this route I could increase the capacity
                if out_edge_sid in probed:
                    previous_capacity = probed[out_edge_sid].capacity_msat
                    if potential_next_probe_capacity > previous_capacity:
                        plugin.log(
                            f"Adding {out_edge_sid} to pending probes. Reason: Current {previous_capacity} potential {potential_next_probe_capacity}")
                        add_pending_probe(dest, p, pending_probes, out_edge_sid)
                        continue

                # I have a pending probe, but though a different route
                # TODO Check properly if the route is different
                opt_pending_probe = get_pending_probe(pending_probes, out_edge_sid)
                if opt_pending_probe is not None and opt_pending_probe['previous_route'] != designated_route:
                    plugin.log(
                        f"Adding {out_edge_sid} to pending probes. Reason: Pending through a different route")
                    add_pending_probe(dest, p, pending_probes, out_edge_sid)
                    continue

        except NodeNotReachable as e:
            e.probe.finished_at = time()
            probed[sid] = e.probe
            plugin.log("No route to the destination node")
        except ZeroCapacity as e:
            e.probe.finished_at = time()
            probed[sid] = e.probe
            plugin.log("No Capacity on this route")

    # Transfers the results of the probing to a Graph
    probes_list = [p for (k, p) in probed.items()]
    node_attributes = probes_to_node_attributes_multiple(my_node_id, probes_list)
    nx.set_node_attributes(G, node_attributes)
    channel_attributes = probes_to_edge_attributes_multiple(G, channels, probes_list, plugin)
    nx.set_edge_attributes(G, channel_attributes)

    # Persist the graph and give the user some feedback
    nx.write_gpickle(G, f"probe_channels_distance_{distance}.gpickle")
    plugin.log("Completed!")

    return probed


def add_pending_probe(dest, p, pending_probes, sid):
    pending_probes.append({
        "dest_node_id": dest,
        "short_channel_id": sid,
        "route_known_capacity": p.capacity_msat,
        "previous_route": p.selected_route
    })


@p.method('probe-capacity')
def on_probe_capacity(plugin, dest_node_id):
    """
    Determine the capacity of the route towards a specific node
    :param plugin:
    :param dest_node_id:
    :return:
    """
    try:
        probe = probe_capacity(plugin=plugin,
                               dest_node_id=dest_node_id)
        plugin.log(
            f"Capacity of the path is roughly {probe.capacity_usd:.2f} USD")
        return probe.to_json()
    except NodeNotReachable:
        plugin.log(
            f"There is not route to the node even with the minimum capacity {MIN_LN_CHANNEL_CAPACITY}")


@p.method('probe-neighbours')
def on_probe_neighbours(plugin, distance=1):
    """
    Perform a probing attack on the neighbours (nodes you have a channel with)
    :param distance: Maximum distance from the source node
    :param plugin:
    :return:
    """
    my_node_id = get_my_node_id(plugin.rpc)

    t1 = time()

    # Init
    channels = plugin.rpc.listchannels()['channels']
    G = channels_to_graph(channels)
    nodes_to_probe = nx.single_source_shortest_path_length(G, source=my_node_id, cutoff=distance)
    G = obtain_subgraph(G, nodes_to_probe)
    payment_hash = ''.join(choice(string.hexdigits) for _ in range(64))
    nodes_count = len(list(G.nodes())) - 1
    plugin.log(f"Neighbours [{nodes_count}]. Estimated time {str(timedelta(seconds=nodes_count * 2))} ")

    # Perform a payment to each node
    probes = []
    i = 0
    for n in G.nodes:
        i = i + 1
        if n == my_node_id:  # Do not probe myself
            continue

        # Try to find a valid route to the node
        probe = Probe(destination=n, payment_hash=payment_hash)
        try:
            probe.selected_route = select_route(plugin=plugin, node_id=n, msatoshi=DEFAULT_PROBE_AMOUNT)
            try_route(plugin=plugin, probe=probe, exclude_erring=True)
        except RpcError:  # Cannot find a route to host or something is wrong in the RPC request
            probe.failcode = NO_ROUTE_TO_HOST

        probes.append(probe)
        plugin.log(f"Probing {n}, {i} of {nodes_count}")

    # Transfers the results of the probing to a Graph
    node_attributes = probes_to_node_attributes(my_node_id, probes)
    nx.set_node_attributes(G, node_attributes)
    channel_attributes = probes_to_edge_attributes(G, channels, probes)
    nx.set_edge_attributes(G, channel_attributes)

    # Persist the graph and give the user some feedback
    nx.write_gpickle(G, f"probe_distance_{distance}.gpickle")
    t2 = time()
    elapsed = t2 - t1
    plugin.log("Completed!")
    return f"Probed {nodes_count} successfully in {str(timedelta(seconds=elapsed))} ({elapsed / nodes_count} s/node)"


@p.init()
def init(configuration, options, plugin):
    plugin.probe_exclusion_duration = int(options['probe-exclusion-duration'])


p.add_option(
    'probe-exclusion-duration',
    '1800',
    'How many seconds should temporarily failed channels be excluded?'
)
p.run()
