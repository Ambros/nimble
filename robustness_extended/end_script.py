import numpy as np
import matplotlib.pyplot as plt
import statistics
import pandas as pd
import networkx as nx
import random
import copy
import datetime
import powerlaw
import collections

###################
# SET UP THE GRAPHS
###################
# open Litecoin lightening network

GL = nx.read_edgelist("channels_2019-11-26_16_00_LC.edgelist")
len(GL)  # get the number of nodes
GL.size()  # get the number of edges

# open Bitcoin lightening network

GB = nx.read_edgelist("channels_2019-11-26_07_00_active_BC.edgelist")
len(GB)  # get the number of nodes
GB.size()  # get the number of edges

# set up an ER network with same number of nodes and same average degree
degree_sequence_btc = sorted([d for n, d in GB.degree()], reverse=True)  # get the degrees of each node
N, m = GB.number_of_nodes(), GB.number_of_edges()  # get the number of nodes and the number of edges
k = np.mean(degree_sequence_btc)  # compute the average degree of the network
p = k / N  # compute p = average degree / number of degrees
ER = nx.erdos_renyi_graph(N, p)  # ER graph with same parameters as GB
BA = nx.barabasi_albert_graph(N, 7)  # average degree below avg. degree of GB


######################
# FUNCTION DEFINITIONS
######################
def betweenness(graph):
    """
    calculates betweenness and plots the 100 nodes with highest betweenness
    """
    content = nx.betweenness_centrality(graph).values()
    list_sorted = sorted(content, reverse=True)

    print("Average: {}".format(sum(list_sorted) / float(len(list_sorted))))
    print("Median:  {}".format(statistics.median(list_sorted)))

    N = 100
    width = 0.8
    ind = np.arange(N)
    plt.bar(ind, list_sorted[:100], width)
    plt.ylabel('Betweenness centality')
    plt.title('100 nodes with highest betweenness centrality')


def get_degree_distribution_fig(G, figure_name):
    """
    generates the degree distribution charts for the report
    """
    degrees = nx.degree(G)
    degrees_sorted = list(degrees)
    degrees_sorted.sort(key=lambda tup: tup[1], reverse=True)
    degrees_sorted_count = [i[1] for i in degrees_sorted]
    print("Average: {}".format(sum(degrees_sorted_count) / float(len(degrees_sorted_count))))
    print("Median:  {}".format(statistics.median(degrees_sorted_count)))
    ind = np.arange(100)
    fig, ax = plt.subplots()
    degree_distribution = ax.bar(ind, degrees_sorted_count[:100], width=0.9, color="black", edgecolor="none",
                                 align="edge")
    ax.set_ylabel("Node degree k")
    ax.set_xlim(0, 100)
    fig.savefig(f"{figure_name}_degree_distribution.pdf", bbox_inches='tight')


def clustering(graph):
    clustering_data = nx.clustering(graph).values()
    clustering_sorted = sorted(clustering_data)
    clustering_sorted.reverse()
    print(clustering_sorted)
    N = 100
    width = 0.8
    ind = np.arange(N)
    plt.bar(ind, clustering_sorted[:100], width)
    plt.ylabel('Clustering coefficient')
    plt.title('100 nodes with highest clustering coefficient')


def diameter(G, R, BA):
    print("Diameter:")

    copy_G = G.copy()
    remove_single_components(copy_G)

    print("Diameter G: {}".format(nx.diameter(copy_G)))
    print("Diameter ER: {}".format(nx.diameter(R)))
    print("Diameter BA: {}".format(nx.diameter(BA)))


def reachable_nodes(G):
    copy_G = G.copy()
    remove_single_components(copy_G)
    ccs = list(nx.connected_components(copy_G))
    assert (len(ccs) == 1)
    return len(ccs[0])


def share_reachable_nodes(G):
    num_nodes = nx.number_of_nodes(G)
    num_reachable_nodes = reachable_nodes(G)
    return num_reachable_nodes / num_nodes


def remove_single_components(G):
    """
    deletes all nodes except the ones in
    the biggest connected component
    """

    list_conn = list(nx.connected_components(G))
    count = 0
    for entry in list_conn:
        if entry != max(list_conn, key=len):
            for setentry in entry:
                G.remove_node(setentry)
                count += 1


def small_worldness(G, R):
    """
    calculate small-worldness as proposed by Humphries (2015)
    """

    print("Small-Worldness:")

    copy_G = G.copy()
    remove_single_components(copy_G)

    clustering_G = nx.transitivity(copy_G)
    clustering_R = nx.transitivity(R)

    print("Clustering G:            {}".format(clustering_G))
    print("Clustering R:            {}".format(clustering_R))

    avg_path_length_G = nx.average_shortest_path_length(copy_G)
    avg_path_length_R = nx.average_shortest_path_length(R)

    print("Average path length G:   {}".format(avg_path_length_G))
    print("Average path length R:   {}".format(avg_path_length_R))

    gamma_G = clustering_G / clustering_R
    lambda_G = avg_path_length_G / avg_path_length_R
    small_worldness = gamma_G / lambda_G

    print("Gamma G:                 {}".format(gamma_G))
    print("Lambda G:                {}".format(lambda_G))
    print("Small-Worldness:         {}".format(small_worldness))


# Create a function that calulates the key parameters of power-law fit
def get_powerlaw_fitfigure(G, figure_name):
    degree_sequence = sorted([d for n, d in G.degree()], reverse=True)
    # degree_sequence = degree_sequence[:-1]

    fitted = powerlaw.Fit(degree_sequence, discrete=True)
    count = 0

    xmin = fitted.power_law.xmin

    non_tail = []
    tail = []
    for j in degree_sequence[:-1]:
        if j >= xmin:
            tail.append(j)
        else:
            non_tail.append(j)

    n_tail = sum(i >= xmin for i in degree_sequence)
    n = len(degree_sequence)
    print("n_tail: ", n_tail, " / ", n)

    samples = 10000
    for _ in range(0, samples):
        simulated_data = []
        for i in range(0, n):
            if random.random() < (n_tail / n):
                next = fitted.power_law.generate_random(1)[0]
            else:
                next = random.choice(non_tail)
            simulated_data.append(next)

        fitsim = powerlaw.Fit(simulated_data, verbose=False)
        if fitsim.power_law.D >= fitted.power_law.D:
            count = count + 1

    print("Evaluation:")
    print("gamma:                  {}".format(fitted.power_law.alpha))
    print("sigma:                        {}".format(fitted.power_law.sigma))
    print("xmin:                         {}".format(fitted.power_law.xmin))
    print("Kolmogorov distance:          {}".format(fitted.power_law.D))
    print("Anzahl Dist_sim >= Dist_real: {}".format(count))
    print("p: {}".format(count / samples))
    print("===================================")

    # prepare counts for the degree distribution
    degreeCount = collections.Counter(degree_sequence)
    for key, value in degreeCount.items():
        degreeCount[key] = value / len(G.nodes())
    deg, cnt = zip(*degreeCount.items())
    x = np.array(deg)
    y = np.array(cnt)

    # degree distribution plot with power-law fit
    fig2, ax2 = plt.subplots()
    fig2.set_figheight(3.5)
    fig2.set_figwidth(6)
    ax2.set_xscale("log")
    ax2.set_yscale("log")
    ax2.set_ylim(0.0001, 1)
    ax2 = fitted.power_law.plot_pdf(ax=ax2, label="Power-law fit")
    ax2.axvline(xmin, linestyle='--', color='grey')
    ax2.text(xmin, 0.0001, f"k min = {xmin}", rotation=90)
    plot_freq, = ax2.loglog(x, y, 'o', markersize=2, label="Relative Frequency")
    ax2.set_xlabel("Node Degree")
    ax2.set_ylabel("P(k)")
    ax2.legend()
    fig2.savefig(f"{figure_name}_degree_distribution_powerlaw.pdf")


def conclusion(G):
    """
    generates a general overview over some key metrics
    """

    print("Overview:")
    print("central point: {}".format(max(list(nx.betweenness_centrality(G).values()))))
    print("Node count: {}".format(len(list(G.nodes))))
    print("Edge count: {}".format(len(list(G.edges))))

    copy = G.copy()
    print("Diameter: {}".format(nx.diameter(copy)))
    print("AVG shortest path length: {}".format(nx.average_shortest_path_length(copy)))


########################
# DESCRIPTIVE STATISTICS
########################
# basic information on the network
connected_components_btc = nx.connected_components(GB)
core_btc = next(nx.connected_components(GB))
connected_components_ltc = nx.connected_components(GL)
core_ltc = next(nx.connected_components(GL))

# more detailed information
print(
    f"Characteristics of \n BTC {nx.info(GB)}; \n\n\n LTC {nx.info(GL)} \n\n\n\n\n ...core lengths BTC: {len(core_btc)}; Core length LTC: {len(core_ltc)}")

diameter(GB, ER, BA)
diameter(GL, ER, BA)

small_worldness(GB, ER)
small_worldness(GL, ER)

betweenness(GB)  # is an indicator of highly central nodes in networks

clustering(GL)
clustering(GB)

conclusion(GL)
conclusion(GB)

# .............Figure 2: Node Degrees (top 100)................................
get_degree_distribution_fig(GL, figure_name="LTC")
get_degree_distribution_fig(GB, figure_name="BTC")

# ..............Figure 2: Power Law fit (plot and infos)........................
get_powerlaw_fitfigure(GL, figure_name="LTC")
get_powerlaw_fitfigure(GB, figure_name="BTC")


############################
# ATTACKS ON NETWORKS
############################

#### General functions

# create a function that removes edges below a certain capacity

def remove_below_cap(G, amount):
    new_G = copy.deepcopy(G)  # copy of the original graph
    edges = G.edges()  # edges of the graph
    tbr = []
    for src, dest in edges:  # get capacity of edges
        edge_data = G.get_edge_data(src, dest)
        cap = edge_data.get('capacity')
        if int(cap) < amount:  # if capacity is lower than the decided amount...
            tbr.append({src, dest})  # ... add the edge to the tbr list
    for src, dest in tbr:  # remove all the edges in the tbr list
        new_G.remove_edge(src, dest)
    return new_G


# create a function that returns true if a path exists and false if a path does not exist

def check_path(G, s, t):
    try:
        nx.shortest_path(G, s, t)  # if it exists a path between nodes s and t...
        return True  # ... return True
    except:
        return False


# create a function for the random attacks on the random networks (ER/BA)

def remove_rand_nodes_fake(G, n):
    # get important information about the graph
    nodes = list(G.nodes)  # nodes

    # remove random n nodes
    random.shuffle(nodes)

    all_reachable_nodes = []
    for i in range(1, n):
        # metric 1: number of reachable nodes
        tbr = nodes[:i]  # select the first n random nodes
        new_G = copy.deepcopy(G)  # copy of the original graph
        new_G.remove_nodes_from(tbr)  # remove nodes

        giant = max(nx.connected_components(new_G), key=len)  # biggest connected component
        reachable_nodes = len(giant)  # number of reachable nodes
        all_reachable_nodes.append(reachable_nodes)

    return all_reachable_nodes


# create a function that removes randomly chosen nodes and calculates the network's metrics

def remove_rand_nodes(G, n, cap):
    # get important information about the graph
    nodes = list(G.nodes)  # nodes
    min_cap = int(min(cap))  # minimum capacity of all edges
    max_cap = int(np.percentile(cap, 25))  # 25% percentile of capacity of edges

    # remove random n nodes
    random.shuffle(nodes)

    all_reachable_nodes = []
    all_avg_max_flows = []
    all_ratios = []
    for i in range(1, n):

        # metric 1: number of reachable nodes
        tbr = nodes[:i]  # select the first n random nodes
        new_G = copy.deepcopy(G)  # copy of the original graph
        new_G.remove_nodes_from(tbr)  # remove nodes

        giant = max(nx.connected_components(new_G), key=len)  # biggest connected component
        reachable_nodes = len(giant)  # number of reachable nodes
        all_reachable_nodes.append(reachable_nodes)

        # metric 2: average maximum flow in sample of 80 edges
        new_nodes = list(new_G.nodes)  # nodes of the new graph
        sum_max_flow = 0
        for i in range(1, 100):  # sample of 80 edges (>10% of the actual nodes)
            s = random.choice(new_nodes)  # get two random nodes
            t = random.choice(new_nodes)
            while (s == t):
                t = random.choice(new_nodes)
            max_flow = nx.maximum_flow_value(new_G, s, t, capacity='capacity')  # maximum flow between nodes
            sum_max_flow = sum_max_flow + max_flow  # sum of maximum flows
            print(datetime.datetime.now())
        avg_max_flow = sum_max_flow / 100  # average maximum flows
        all_avg_max_flows.append(avg_max_flow)

        # metric 3: payment success ratio
        poss_amounts = range(min_cap, max_cap)
        sum_success = 0
        for i in range(1, 100):  # sample size??
            amount = random.choice(poss_amounts)
            new_new_G = remove_below_cap(G, amount)
            new_new_nodes = list(new_new_G.nodes)
            s = random.choice(new_new_nodes)  # get two random nodes
            t = random.choice(new_new_nodes)
            while (s == t):
                t = random.choice(new_new_nodes)
            if check_path(new_new_G, s, t):  # if it exists a path between s and t...
                sum_success = sum_success + 1  # ... add one to the count
                print(datetime.datetime.now())
            else:
                sum_success = sum_success + 0
                print(datetime.datetime.now())
        ratio = sum_success / 100  # compute the payment success ratio
        all_ratios.append(ratio)

    return all_reachable_nodes, all_avg_max_flows, all_ratios


# create a function that removes degree central nodes and calculates the network's metrics

def remove_central_nodes_d(G, n, cap, centralities):
    min_cap = int(min(cap))  # minimum capacity of all edges
    max_cap = int(np.percentile(cap, 25))  # 25% percentile of capacity of edges

    # remove degree central n nodes
    central = centralities.sort_values(by=['degree'], ascending=False)

    all_reachable_nodes = []
    all_avg_max_flows = []
    all_ratios = []
    for i in range(1, n):

        # metric 1: number of reachable nodes
        nodes = central['nodes'].tolist()  # nodes
        tbr = nodes[:i]  # select the first n random nodes
        new_G = copy.deepcopy(G)  # copy of the original graph
        new_G.remove_nodes_from(tbr)  # remove nodes

        giant = max(nx.connected_components(new_G), key=len)  # biggest connected component
        reachable_nodes = len(giant)  # number of reachable nodes
        all_reachable_nodes.append(reachable_nodes)

        # metric 2: average maximum flow in sample of 80 edges
        new_nodes = list(new_G.nodes)  # nodes of the new graph
        sum_max_flow = 0
        for i in range(1, 100):  # sample of 80 edges (>10% of the actual nodes)
            s = random.choice(new_nodes)  # get two random nodes
            t = random.choice(new_nodes)
            while (s == t):
                t = random.choice(new_nodes)
            max_flow = nx.maximum_flow_value(new_G, s, t, capacity='capacity')  # maximum flow between nodes
            sum_max_flow = sum_max_flow + max_flow  # sum of maximum flows
            print(datetime.datetime.now())
        avg_max_flow = sum_max_flow / 100  # average maximum flows
        all_avg_max_flows.append(avg_max_flow)

        # metric 3: payment success ratio
        poss_amounts = range(min_cap, max_cap)
        sum_success = 0
        for i in range(1, 100):  # sample size??
            amount = random.choice(poss_amounts)
            new_new_G = remove_below_cap(G, amount)
            new_new_nodes = list(new_new_G.nodes)
            s = random.choice(new_new_nodes)  # get two random nodes
            t = random.choice(new_new_nodes)
            while (s == t):
                t = random.choice(new_new_nodes)
            if check_path(new_new_G, s, t):  # if it exists a path between s and t...
                sum_success = sum_success + 1  # ... add one to the count
                print(datetime.datetime.now())
            else:
                sum_success = sum_success + 0
                print(datetime.datetime.now())
        ratio = sum_success / 100  # compute the payment success ratio
        all_ratios.append(ratio)

    return all_reachable_nodes, all_avg_max_flows, all_ratios


# create a function that removes betweenness central nodes and calculates the network's metrics

def remove_central_nodes_b(G, n, cap, centralities):
    min_cap = int(min(cap))  # minimum capacity of all edges
    max_cap = int(np.percentile(cap, 25))  # 25% percentile of capacity of edges

    # remove betweenness central n nodes
    central = centralities.sort_values(by=['between'], ascending=False)

    all_reachable_nodes = []
    all_avg_max_flows = []
    all_ratios = []
    for i in range(1, n):

        # metric 1: number of reachable nodes
        nodes = central['nodes'].tolist()  # nodes
        tbr = nodes[:i]  # select the first n random nodes
        new_G = copy.deepcopy(G)  # copy of the original graph
        new_G.remove_nodes_from(tbr)  # remove nodes

        giant = max(nx.connected_components(new_G), key=len)  # biggest connected component
        reachable_nodes = len(giant)  # number of reachable nodes
        all_reachable_nodes.append(reachable_nodes)

        # metric 2: average maximum flow in sample of 80 edges
        new_nodes = list(new_G.nodes)  # nodes of the new graph
        sum_max_flow = 0
        for i in range(1, 100):  # sample of 80 edges (>10% of the actual nodes)
            s = random.choice(new_nodes)  # get two random nodes
            t = random.choice(new_nodes)
            while (s == t):
                t = random.choice(new_nodes)
            max_flow = nx.maximum_flow_value(new_G, s, t, capacity='capacity')  # maximum flow between nodes
            sum_max_flow = sum_max_flow + max_flow  # sum of maximum flows
            print(datetime.datetime.now())
        avg_max_flow = sum_max_flow / 100  # average maximum flows
        all_avg_max_flows.append(avg_max_flow)

        # metric 3: payment success ratio
        poss_amounts = range(min_cap, max_cap)
        sum_success = 0
        for i in range(1, 100):  # sample size??
            amount = random.choice(poss_amounts)
            new_new_G = remove_below_cap(G, amount)
            new_new_nodes = list(new_new_G.nodes)
            s = random.choice(new_new_nodes)  # get two random nodes
            t = random.choice(new_new_nodes)
            while (s == t):
                t = random.choice(new_new_nodes)
            if check_path(new_new_G, s, t):  # if it exists a path between s and t...
                sum_success = sum_success + 1  # ... add one to the count
                print(datetime.datetime.now())
            else:
                sum_success = sum_success + 0
                print(datetime.datetime.now())
        ratio = sum_success / 100  # compute the payment success ratio
        all_ratios.append(ratio)

    return all_reachable_nodes, all_avg_max_flows, all_ratios


# create a function that removes eigenvector central nodes and calculates the network's metrics

def remove_central_nodes_e(G, n, cap, centralities):
    min_cap = int(min(cap))  # minimum capacity of all edges
    max_cap = int(np.percentile(cap, 25))  # 25% percentile of capacity of edges

    # remove eigenvector central n nodes
    central = centralities.sort_values(by=['eigen'], ascending=False)

    all_reachable_nodes = []
    all_avg_max_flows = []
    all_ratios = []
    for i in range(1, n):

        # metric 1: number of reachable nodes
        nodes = central['nodes'].tolist()  # nodes
        tbr = nodes[:i]  # select the first n random nodes
        new_G = copy.deepcopy(G)  # copy of the original graph
        new_G.remove_nodes_from(tbr)  # remove nodes

        giant = max(nx.connected_components(new_G), key=len)  # biggest connected component
        reachable_nodes = len(giant)  # number of reachable nodes
        all_reachable_nodes.append(reachable_nodes)

        # metric 2: average maximum flow in sample of 80 edges
        new_nodes = list(new_G.nodes)  # nodes of the new graph
        sum_max_flow = 0
        for i in range(1, 100):  # sample of 80 edges (>10% of the actual nodes)
            s = random.choice(new_nodes)  # get two random nodes
            t = random.choice(new_nodes)
            while (s == t):
                t = random.choice(new_nodes)
            max_flow = nx.maximum_flow_value(new_G, s, t, capacity='capacity')  # maximum flow between nodes
            sum_max_flow = sum_max_flow + max_flow  # sum of maximum flows
            avg_max_flow = sum_max_flow / 100  # average maximum flows
            print(datetime.datetime.now())
        all_avg_max_flows.append(avg_max_flow)

        # metric 3: payment success ratio
        poss_amounts = range(min_cap, max_cap)
        sum_success = 0
        for i in range(1, 100):  # sample size??
            amount = random.choice(poss_amounts)
            new_new_G = remove_below_cap(G, amount)
            new_new_nodes = list(new_new_G.nodes)
            s = random.choice(new_new_nodes)  # get two random nodes
            t = random.choice(new_new_nodes)
            while (s == t):
                t = random.choice(new_new_nodes)
            if check_path(new_new_G, s, t):  # if it exists a path between s and t...
                sum_success = sum_success + 1  # ... add one to the count
                print(datetime.datetime.now())
            else:
                sum_success = sum_success + 0
                print(datetime.datetime.now())
        ratio = sum_success / 100  # compute the payment success ratio
        all_ratios.append(ratio)

    return all_reachable_nodes, all_avg_max_flows, all_ratios


# create a function that removes highest ranked minimum cut nodes and calculates the network's metrics

def remove_mincut_nodes(G, n, cap, centralities):
    min_cap = int(min(cap))  # minimum capacity of all edges
    max_cap = int(np.percentile(cap, 25))  # 25% percentile of capacity of edges

    # remove highest ranked minimum cut nodes
    central = centralities.sort_values(by=['mincut'], ascending=False)

    all_reachable_nodes = []
    all_avg_max_flows = []
    all_ratios = []
    for i in range(1, n):

        # metric 1: number of reachable nodes
        nodes = central['nodes'].tolist()  # nodes
        tbr = nodes[:i]  # select the first n random nodes
        new_G = copy.deepcopy(G)  # copy of the original graph
        new_G.remove_nodes_from(tbr)  # remove nodes

        giant = max(nx.connected_components(new_G), key=len)  # biggest connected component
        reachable_nodes = len(giant)  # number of reachable nodes
        all_reachable_nodes.append(reachable_nodes)

        # metric 2: average maximum flow in sample of 80 edges
        new_nodes = list(new_G.nodes)  # nodes of the new graph
        sum_max_flow = 0
        for i in range(1, 100):  # sample of 80 edges (>10% of the actual nodes)
            s = random.choice(new_nodes)  # get two random nodes
            t = random.choice(new_nodes)
            while (s == t):
                t = random.choice(new_nodes)
            max_flow = nx.maximum_flow_value(new_G, s, t, capacity='capacity')  # maximum flow between nodes
            sum_max_flow = sum_max_flow + max_flow  # sum of maximum flows
            print(datetime.datetime.now())
        avg_max_flow = sum_max_flow / 100  # average maximum flows
        all_avg_max_flows.append(avg_max_flow)

        # metric 3: payment success ratio
        poss_amounts = range(min_cap, max_cap)
        sum_success = 0
        for i in range(1, 100):  # sample size??
            amount = random.choice(poss_amounts)
            new_new_G = remove_below_cap(G, amount)
            new_new_nodes = list(new_new_G.nodes)
            s = random.choice(new_new_nodes)  # get two random nodes
            t = random.choice(new_new_nodes)
            while (s == t):
                t = random.choice(new_new_nodes)
            if check_path(new_new_G, s, t):  # if it exists a path between s and t...
                sum_success = sum_success + 1  # ... add one to the count
                print(datetime.datetime.now())
            else:
                sum_success = sum_success + 0
                print(datetime.datetime.now())
        ratio = sum_success / 100  # compute the payment success ratio
        all_ratios.append(ratio)

    return all_reachable_nodes, all_avg_max_flows, all_ratios


#### Litecoin

# calculate the different centrality measures

ltc_nodes = list(nx.degree_centrality(GL).keys())
ltc_degree = list(nx.degree_centrality(GL).values())
ltc_between = list(nx.betweenness_centrality(GL).values())
ltc_eigen = list(nx.eigenvector_centrality(GL).values())

# store the centralities in a dataframe

ltc_centralities = pd.DataFrame()
ltc_centralities["nodes"] = ltc_nodes
ltc_centralities["degree"] = ltc_degree
ltc_centralities["between"] = ltc_between
ltc_centralities["eigen"] = ltc_eigen

# calculate the nodes' rank based on whether they are part of minimum cut edges

ltc_centralities['mincut'] = 0
for i in range(1, 100):
    s = random.choice(ltc_nodes)
    t = random.choice(ltc_nodes)
    while (s == t):
        t = random.choice(ltc_nodes)
    try:
        edgeset = list(nx.minimum_edge_cut(GL, s, t))
        ltc_centralities.mincut[ltc_centralities.nodes == edgeset[0][0]] = ltc_centralities.mincut[
                                                                               ltc_centralities.nodes == edgeset[0][
                                                                                   0]] + 1
        ltc_centralities.mincut[ltc_centralities.nodes == edgeset[0][1]] = ltc_centralities.mincut[
                                                                               ltc_centralities.nodes == edgeset[0][
                                                                                   1]] + 1
    except:
        pass

# get the capacity of the graph

ltc_cap = []
for src, dest in GL.edges:  # capacity of all edges
    edge_data = GL.get_edge_data(src, dest)
    ltc_cap.append(edge_data.get('capacity'))

# attack the graph
ltc_random_res = remove_rand_nodes(GL, 11, ltc_cap)
ltc_degree_res = remove_central_nodes_d(GL, 11, ltc_cap, ltc_centralities)
ltc_between_res = remove_central_nodes_b(GL, 11, ltc_cap, ltc_centralities)
ltc_eigen_res = remove_central_nodes_e(GL, 11, ltc_cap, ltc_centralities)
ltc_mincut_res = remove_mincut_nodes(GL, 11, ltc_cap, ltc_centralities)

# separate the tuples to single lists

ltc_random_rn = ltc_random_res[0]
ltc_random_amf = ltc_random_res[1]
ltc_random_psr = ltc_random_res[2]

ltc_degree_rn = ltc_degree_res[0]
ltc_degree_amf = ltc_degree_res[1]
ltc_degree_psr = ltc_degree_res[2]

ltc_between_rn = ltc_between_res[0]
ltc_between_amf = ltc_between_res[1]
ltc_between_psr = ltc_between_res[2]

ltc_eigen_rn = ltc_eigen_res[0]
ltc_eigen_amf = ltc_eigen_res[1]
ltc_eigen_psr = ltc_eigen_res[2]

ltc_mincut_rn = ltc_mincut_res[0]
ltc_mincut_amf = ltc_mincut_res[1]
ltc_mincut_psr = ltc_mincut_res[2]

#### Bitcoin

# caBCulate the different centrality measures

btc_nodes = list(nx.degree_centrality(GB).keys())
btc_degree = list(nx.degree_centrality(GB).values())
btc_between = list(nx.betweenness_centrality(GB).values())
btc_eigen = list(nx.eigenvector_centrality(GB).values())

# store the centralities in a dataframe

btc_centralities = pd.DataFrame()
btc_centralities["nodes"] = btc_nodes
btc_centralities["degree"] = btc_degree
btc_centralities["between"] = btc_between
btc_centralities["eigen"] = btc_eigen

# caBCulate the nodes' rank based on whether they are part of minimum cut edges

btc_centralities['mincut'] = 0
for i in range(1, 500):
    s = random.choice(btc_nodes)
    t = random.choice(btc_nodes)
    while (s == t):
        t = random.choice(btc_nodes)
    try:
        edgeset = list(nx.minimum_edge_cut(GB, s, t))
        for j in edgeset:
            btc_centralities.mincut[btc_centralities.nodes == j[0]] = btc_centralities.mincut[
                                                                          btc_centralities.nodes == j[0]] + 1
            btc_centralities.mincut[btc_centralities.nodes == j[1]] = btc_centralities.mincut[
                                                                          btc_centralities.nodes == j[1]] + 1
    except:
        pass

# get the capacity of the graph

btc_cap = []
for src, dest in GB.edges:  # capacity of all edges
    edge_data = GB.get_edge_data(src, dest)
    btc_cap.append(edge_data.get('capacity'))

# attack the graph
btc_random_res = remove_rand_nodes(GB, 200, btc_cap)
btc_degree_res = remove_central_nodes_d(GB, 200, btc_cap, btc_centralities)
btc_between_res = remove_central_nodes_b(GB, 200, btc_cap, btc_centralities)
btc_eigen_res = remove_central_nodes_e(GB, 200, btc_cap, btc_centralities)
btc_mincut_res = remove_mincut_nodes(GB, 200, btc_cap, btc_centralities)
# separate the tuples to sinGBe lists

btc_random_rn = btc_random_res[0]
btc_random_amf = btc_random_res[1]
btc_random_psr = btc_random_res[2]

btc_degree_rn = btc_degree_res[0]
btc_degree_amf = btc_degree_res[1]
btc_degree_psr = btc_degree_res[2]

btc_between_rn = btc_between_res[0]
btc_between_amf = btc_between_res[1]
btc_between_psr = btc_between_res[2]

btc_eigen_rn = btc_eigen_res[0]
btc_eigen_amf = btc_eigen_res[1]
btc_eigen_psr = btc_eigen_res[2]

btc_mincut_rn = btc_mincut_res[0]
btc_mincut_amf = btc_mincut_res[1]
btc_mincut_psr = btc_mincut_res[2]

#### ER Network

# attack the graph
er_random_rn = remove_rand_nodes_fake(ER, 200)

#### BA Network

# attack the graph
ba_random_rn = remove_rand_nodes_fake(BA, 200)

# robustness: comparison Small world, scale free, bitcoin

### reachable nodes BTC
x = np.arange(1, 200)
plt.plot(x, er_random_rn, label="ER network")
plt.plot(x, ba_random_rn, label="BA network")
plt.plot(x, btc_random_rn, label="Bitcoin network")
plt.xlabel("Number of removed nodes")
plt.ylabel("Number of reachable components")
plt.legend()
plt.savefig('plots/robustness_reachable_nodes.pdf')

### reachable nodes LTC
xx = np.arange(0, 10)
plt.plot(xx, ltc_random_rn, label="Litecoin network")
plt.xlabel("Number of removed nodes")
plt.ylabel("Number of reachable components")
plt.legend()
plt.savefig('plots/robustness_reachable_nodes_ltc.pdf')

# percolation: comparison of different targeted attack types on bitcoin and litecoin network

### reachable nodes BTC
giant_GB = max(nx.connected_components(GB))
giant_GB = len(giant_GB)
y = []
for i in range(1, 200):
    y_i = giant_GB - i
    y.append(y_i)

plt.plot(x, btc_degree_rn, label="degree", linestyle='--')
plt.plot(x, btc_between_rn, label="betweenness", linestyle='--')
plt.plot(x, btc_eigen_rn, label="eigenvector", linestyle='--')
plt.plot(x, btc_mincut_rn, label="minimum cut")
plt.plot(x, y, label="ideal curve", color="black")
plt.xlabel("Number of removed nodes")
plt.ylabel("Number of reachable nodes")
plt.legend()
plt.savefig('plots/reachable_nodes_targeted_btc.pdf')

### reachable nodes LTC
giant_GL = max(nx.connected_components(GL))
giant_GL = len(giant_GL)
y = []
for i in range(0, 10):
    y_i = giant_GL - i
    y.append(y_i)

plt.plot(xx, ltc_degree_rn, label="degree", linestyle='--')
plt.plot(xx, ltc_between_rn, label="betweenness", linestyle='--')
plt.plot(xx, ltc_eigen_rn, label="eigenvector", linestyle='--')
plt.plot(xx, ltc_mincut_rn, label="minimum cut")
plt.plot(xx, y, label="ideal curve", color="black")
plt.xlabel("Number of removed nodes")
plt.ylabel("Number of reachable nodes")
plt.legend()
plt.savefig('plots/reachable_nodes_targeted_ltc.pdf')

### average maximum flow BTC
plt.plot(x, btc_degree_amf, label="degree", linestyle='--')
plt.plot(x, btc_between_amf, label="betweenness", linestyle='--')
plt.plot(x, btc_eigen_amf, label="eigenvector", linestyle='--')
plt.plot(x, btc_mincut_amf, label="minimum cut")
plt.xlabel("Number of removed nodes")
plt.ylabel("Average maximum flow")
plt.legend()
plt.savefig('plots/avg_max_flow_targeted_btc.pdf')

### average maximum flow LTC
plt.plot(xx, ltc_degree_amf, label="degree", linestyle='--')
plt.plot(xx, ltc_between_amf, label="betweenness", linestyle='--')
plt.plot(xx, ltc_eigen_amf, label="eigenvector", linestyle='--')
plt.plot(xx, ltc_mincut_amf, label="minimum cut")
plt.xlabel("Number of removed nodes")
plt.ylabel("Average maximum flow")
plt.legend()
plt.savefig('plots/avg_max_flow_targeted_ltc.pdf')

### payment success ratio BTC
plt.plot(x, btc_degree_psr, label="degree", linestyle='--')
plt.plot(x, btc_between_psr, label="betweenness", linestyle='--')
plt.plot(x, btc_eigen_psr, label="eigenvector", linestyle='--')
plt.plot(x, btc_mincut_psr, label="minimum cut")
plt.xlabel("Number of removed nodes")
plt.ylabel("Payment success ratio")
plt.legend()
plt.savefig('plots/payment_success_targeted_btc.pdf')

### payment success ratio LTC
plt.plot(xx, ltc_degree_psr, label="degree", linestyle='--')
plt.plot(xx, ltc_between_psr, label="betweenness", linestyle='--')
plt.plot(xx, ltc_eigen_psr, label="eigenvector", linestyle='--')
plt.plot(xx, ltc_mincut_psr, label="minimum cut")
plt.xlabel("Number of removed nodes")
plt.ylabel("Payment success ratio")
plt.legend()
plt.savefig('plots/payment_success_targeted_ltc.pdf')


### relative reachable nodes plot

# define function to calculate the percentage
def get_per(result, G):
    per_list = []
    giant = max(nx.connected_components(G))
    giant_len = len(giant)
    for i in result:
        per_i = i / giant_len
        per_list.append(per_i)
    return (per_list)


# transform the absolute results in percentages by using the formula

# litecoin
ltc_between_rn_per = get_per(ltc_between_rn, GL)
ltc_degree_rn_per = get_per(ltc_degree_rn, GL)
ltc_eigen_rn_per = get_per(ltc_eigen_rn, GL)
ltc_mincut_rn_per = get_per(ltc_mincut_rn, GL)

# bitcooin
btc_between_rn_per = get_per(btc_between_rn, GB)
btc_degree_rn_per = get_per(btc_degree_rn, GB)
btc_eigen_rn_per = get_per(btc_eigen_rn, GB)
btc_mincut_rn_per = get_per(btc_mincut_rn, GB)

# xs
xx_per = []
for i in xx:
    xx_i = i / giant_GL
    xx_per.append(xx_i)

x_per = []
for i in x:
    x_i = i / giant_GB
    x_per.append(x_i)

# plot
from matplotlib.lines import Line2D

cl = [Line2D([0], [0], color="royalblue", lw=2),
      Line2D([0], [0], color="orange", lw=2)]
lt = [Line2D([0], [0], linestyle="--", color="black", lw=2),
      Line2D([0], [0], linestyle="-.", color="black", lw=2),
      Line2D([0], [0], linestyle=":", color="black", lw=2),
      Line2D([0], [0], linestyle="-", color="black", lw=2)]

plt.plot(xx_per, ltc_between_rn_per, label="LTC: betweenness", color="royalblue", linestyle="--")
plt.plot(xx_per, ltc_degree_rn_per, label="LTC: degree", color="royalblue", linestyle="-.")
plt.plot(xx_per, ltc_eigen_rn_per, label="LTC: eigenvector", color="royalblue", linestyle=":")
plt.plot(xx_per, ltc_mincut_rn_per, label="LTC: minimum cut", color="royalblue", linestyle="-")
plt.plot(x_per, btc_between_rn_per, label="BTC: betweenness", color="orange", linestyle="--")
plt.plot(x_per, btc_degree_rn_per, label="BTC: degree", color="orange", linestyle="-.")
plt.plot(x_per, btc_eigen_rn_per, label="BTC: eigenvector", color="orange", linestyle=":")
plt.plot(x_per, btc_mincut_rn_per, label="BTC: minimum cut", color="orange", linestyle="-")
plt.xlabel("Proportion of the removed nodes")
plt.ylabel("Proportion of the reachable nodes")
leg1 = plt.legend(cl, ['Bitcoin', 'Litecoin'])
leg2 = plt.legend(lt, ['betweenness', 'degree', 'eigenvector', 'minimum cut'], loc=3)
plt.gca().add_artist(leg1)
plt.savefig('plots/run_2/reachable_nodes_relative.pdf')
