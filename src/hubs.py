import sys

import networkx as nx
import matplotlib.pyplot as plt
import statistics
from collections import Counter

if __name__ == '__main__':

    if len(sys.argv) != 2:
        print("Usage: python3 hubs.py <input_file_path>")
        sys.exit(1)

    input_file_path = sys.argv[1]

    G = nx.read_edgelist(input_file_path,
                         create_using=nx.Graph)  # TODO Decide if we want to consider it directional or bidirectional
    print(nx.info(G))
    degree_sequence = [G.degree(n) for n in G.nodes]
    highest_degree_node = max(G.nodes, key=G.degree)
    lowest_degree_node = min(G.nodes, key=G.degree)

    # TODO Remove
    print('IsConnected:', nx.is_connected(G))
    mean_capacity = statistics.mean(e[2] for e in G.edges.data('capacity', default=1))
    print(f"Average channel capacity: {mean_capacity} sats ({mean_capacity / 100_000 * 10.09790509} USD)")
    print('Median degree:', statistics.median(degree_sequence))
    print("Highest degree node", highest_degree_node, ", Degree:", G.degree(highest_degree_node))
    print("Lowest degree node", lowest_degree_node, ", Degree:", G.degree(lowest_degree_node))
    core = nx.k_core(G)
    print(f"Core size {len(core)}")

    betweenness = nx.centrality.betweenness_centrality(G)
    highest_betweenness_node = max(G.nodes, key=betweenness.get)
    print("Highest Betweenness node", highest_betweenness_node, ", Betweenness:", G.degree(highest_betweenness_node))

    betweenness_sequence = list(betweenness.values())
    print('Mean betweenness:', statistics.mean(betweenness_sequence))
    print('Median betweenness:', statistics.median(betweenness_sequence))

    degree_counts = Counter(degree_sequence)
    print("Degree counts", degree_counts)
    min_degree, max_degree = min(degree_counts.keys()), max(degree_counts.keys())

    plot_x = list(range(min_degree, max_degree + 1))
    plot_y = [degree_counts.get(x, 0) for x in plot_x]

    plt.bar(plot_x, plot_y)
    plt.xlabel('Degree', fontsize=16)
    plt.ylabel('# of nodes', fontsize=16)
    plt.title('Barchart - Number of nodes with a specific degree', fontsize=20)
    plt.show()
