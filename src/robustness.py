# %%

import random
import networkx as nx
import matplotlib.pyplot as plt
import statistics
from collections import Counter

dt = "2019-08-23_12:00"
file_name = f'./../snapshots/mainnet/edges/edges_undirected{dt}.edgelist'

G = nx.read_edgelist(file_name)
print("Network:", file_name)
print(nx.info(G))

connected_components = nx.connected_components(G)
core = next(nx.connected_components(G))
print('Core', core)
print('Core length', len(core))

# Random failure src
C = G.copy()

nodes_to_remove = random.sample(list(C.nodes), 2)
C.remove_nodes_from(nodes_to_remove)

number_of_steps = 25
M = C.number_of_nodes() // number_of_steps
num_nodes_removed = range(0, G.number_of_nodes(), M)

N = G.number_of_nodes()
C = G.copy()
random_attack_core_proportions = []
for nodes_removed in num_nodes_removed:
    # Measure the relative size of the network core
    core = next(nx.connected_components(C))
    core_proportion = len(core) / N
    random_attack_core_proportions.append(core_proportion)

    # If there are more than M nodes, select M nodes at random and remove them
    if C.number_of_nodes() > M:
        nodes_to_remove = random.sample(list(C.nodes), M)
        C.remove_nodes_from(nodes_to_remove)

nodes_sorted_by_degree = sorted(G.nodes, key=G.degree, reverse=True)
top_degree_nodes = nodes_sorted_by_degree[:M]

plt.plot(num_nodes_removed, random_attack_core_proportions, marker='o', label='Failures')


N = G.number_of_nodes()
M = N // number_of_steps

num_nodes_removed = range(0, N, M)
C = G.copy()
targeted_attack_core_proportions = []
for nodes_removed in num_nodes_removed:
    # Measure the relative size of the network core
    core = next(nx.connected_components(C))
    core_proportion = len(core) / N
    targeted_attack_core_proportions.append(core_proportion)

    # If there are more than M nodes, select top M nodes and remove them
    if C.number_of_nodes() > M:
        nodes_sorted_by_degree = sorted(C.nodes, key=C.degree, reverse=True)
        nodes_to_remove = nodes_sorted_by_degree[:M]
        C.remove_nodes_from(nodes_to_remove)

plt.title('Random failure vs. targeted attack')
plt.xlabel('Number of nodes removed')
plt.ylabel('Proportion of nodes in core')
plt.plot(num_nodes_removed, targeted_attack_core_proportions, marker='^', label='Attacks')
plt.legend()
plt.show()